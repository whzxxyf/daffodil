package com.daffodil.cms.service;

import com.daffodil.cms.entity.CmsSiteConfig;

/**
 * 站点配置 服务
 * @author yweijian
 * @date 2020年10月29日
 * @version 1.0
 * @description
 */
public interface ICmsSiteConfigService {

	/**
	 * 查询站点配置信息
	 * @param configId
	 * @return
	 */
	public CmsSiteConfig selectSiteConfigBySiteId(String siteId);

	/**
	 * 新增站点配置
	 * @param config
	 */
	public void insertSiteConfig(CmsSiteConfig config);

	/**
	 * 修改站点配置
	 * @param config
	 */
	public void updateSiteConfig(CmsSiteConfig config);

}
