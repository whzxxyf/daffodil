package com.daffodil.cms.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.annotation.Dict;
import com.daffodil.core.annotation.Hql;
import com.daffodil.core.annotation.Hql.Logical;
import com.daffodil.core.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 站点配置
 * @author yweijian
 * @date 2020年10月29日
 * @version 1.0
 * @description
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "cms_site_config")
public class CmsSiteConfig extends BaseEntity{
	private static final long serialVersionUID = -6635084720435722437L;

	/** 栏目编号 */
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid")
	@Column(name = "config_id")
	private String id;
	
	/** 站点ID */
	@Column(name = "site_id")
	@Hql(type = Logical.EQ)
	private String siteId;
	
	/** 存储地址*/
	@Column(name = "site_path")
	@Hql(type = Logical.LIKE)
	private String sitePath;
	
	/** 站点编码（文件夹名）*/
	@Column(name = "site_code")
	@Hql(type = Logical.EQ)
	private String siteCode;
	
	/** 站点（内部）HTTP地址 */
	@Column(name = "site_url")
	@Hql(type = Logical.LIKE)
	private String siteUrl;
	
	/** 站点（发布）HTTP地址 */
	@Column(name = "publish_url")
	@Hql(type = Logical.LIKE)
	private String publishUrl;
	
	/** 配置状态（0正常 1停用 2删除） */
	@Column(name = "status")
	@Dict(value = "sys_data_status")
	@Hql(type = Logical.EQ)
	private String status;
	
	/** 创建者 */
	@Column(name="create_by")
	@Hql(type = Logical.LIKE)
	private String createBy;

	/** 创建时间 */
	@Column(name="create_time")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	/** 更新者 */
	@Column(name="update_by")
	@Hql(type = Logical.LIKE)
	private String updateBy;

	/** 更新时间 */
	@Column(name="update_time")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date updateTime;

	/** 备注 */
	@Column(name="remark")
	@Hql(type = Logical.LIKE)
	private String remark;
	
	/** 站点信息 */
	@Transient
	private CmsChannel site;
	
}
