package com.daffodil.cms.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 栏目文章关系表
 * @author yweijian
 * @date 2020年10月25日
 * @version 1.0
 * @description
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name="cms_channel_document")
public class CmsChannelDocument extends BaseEntity {
	private static final long serialVersionUID = 1840453654023803981L;

	/** 稿件编号 */
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid")
	@Column(name = "id")
	private String id;
	
	/** 站点ID */
	@Column(name = "site_id")
	private String siteId;
	
	/** 栏目ID */
	@Column(name = "channel_id")
	private String channelId;
	
	/** 文章ID */
	@Column(name = "doc_id")
	private String documentId;
	
	/** 创建者 */
	@Column(name="create_by")
	private String createBy;

	/** 创建时间 */
	@Column(name="create_time")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;
	
}
