package com.daffodil.easyfile.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.daffodil.core.annotation.Hql;
import com.daffodil.core.annotation.Hql.Logical;
import com.daffodil.core.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 文件分片
 * @author yweijian
 * @date 2020年12月23日
 * @version 1.0
 * @description
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "easyfile_chunk")
public class EasyfileChunk extends BaseEntity{
	private static final long serialVersionUID = 5089492212306517639L;
	
	/** 分片MD5唯一标识 */
	@Id
	@Column(name = "chunk_id")
	private String id;
	
	/** 当前分片（从0开始计数） */
	@Column(name= "chunk")
	private Integer chunk;
	
	/** 总分片数 */
	@Column(name= "chunks")
	private Integer chunks;
	
	/** 分片路径 */
	@Column(name= "chunk_path")
	private String chunkPath;
	
	/** 分片开始位置 */
	@Column(name= "start")
	private Integer start;
	
	/** 分片结束位置 */
	@Column(name= "end")
	private Integer end;
	
	/** 文件编号=MD5(chun_id...)*/
	@Column(name= "file_id")
	@Hql(type = Logical.EQ)
	private String fileId;
	
	/** 文件名称 */
	@Column(name= "file_name")
	@Hql(type = Logical.LIKE)
	private String fileName;
	
	/** 文件大小 */
	@Column(name= "file_size")
	private Long fileSize;
	
	/** 文件类型 */
	@Column(name= "file_type")
	private String fileType;
	
	/** 创建者 */
	@Column(name="create_by")
	@Hql(type = Logical.LIKE)
	private String createBy;

	/** 创建时间 */
	@Column(name="create_time")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	/** 更新者 */
	@Column(name="update_by")
	@Hql(type = Logical.LIKE)
	private String updateBy;

	/** 更新时间 */
	@Column(name="update_time")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date updateTime;

	/** 备注 */
	@Column(name="remark")
	@Hql(type = Logical.LIKE)
	private String remark;

}
