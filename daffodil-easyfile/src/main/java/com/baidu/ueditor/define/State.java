package com.baidu.ueditor.define;

/**
 * 处理状态接口
 * @date 2021年2月7日
 * @version 1.0
 * @description
 */
public interface State {

	public boolean isSuccess ();

	public void putInfo( String name, String val );

	public void putInfo ( String name, long val );

	public String toJSONString ();

}
