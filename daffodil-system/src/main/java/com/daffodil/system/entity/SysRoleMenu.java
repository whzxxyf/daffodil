package com.daffodil.system.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.entity.BaseEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 角色和菜单关联
 * 
 * @author yweijian
 * @date 2019年8月16日
 * @version 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "sys_role_menu")
public class SysRoleMenu extends BaseEntity{
	private static final long serialVersionUID = 2749530865139654800L;

	/** 角色和菜单关联编号 */
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid")
	@Column(name = "role_menu_id")
	private String id;
	
	/** 角色ID */
	@Column(name = "role_id")
	private String roleId;

	/** 菜单ID */
	@Column(name = "menu_id")
	private String menuId;
}
