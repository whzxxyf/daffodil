package com.daffodil.system.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.annotation.Dict;
import com.daffodil.core.annotation.Excel;
import com.daffodil.core.annotation.Hql;
import com.daffodil.core.annotation.Hql.Logical;
import com.daffodil.core.entity.BaseEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 操作日志记录表
 * @author yweijian
 * @date 2019年12月16日
 * @version 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "sys_oper_log")
public class SysOperLog extends BaseEntity {
	private static final long serialVersionUID = 2376490068865616568L;

	/** 操作编号 */
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid")
	@Column(name = "oper_log_id")
	@Excel(name = "操作编号")
	private String id;
	
	/** 操作模块 */
	@Column(name = "title")
	@Excel(name = "操作模块")
	@Hql(type = Logical.LIKE)
	private String title;

	/** 业务类型（0其它,1新增,2修改,3删除,4授权,5导出,6导入,7强退,8生成代码,9清空数据） */
	@Column(name = "business_type")
	@Dict(value = "sys_business_type")
	@Excel(name = "业务类型", readConverterExp = "0=其它,1=新增,2=修改,3=删除,4=授权,5=导出,6=导入,7=强退,8=生成代码,9=清空数据")
	@Hql(type = Logical.IN)
	private String businessType;

	/** 请求方法 */
	@Column(name = "method")
	@Excel(name = "请求方法")
	@Hql(type = Logical.EQ)
	private String method;

	/** 操作类别（0其它 1PC端 2移动端） */
	@Column(name = "operator_type")
	@Dict(value = "sys_client_type")
	@Excel(name = "操作类别", readConverterExp = "0=其它,1=PC端,2=移动端")
	@Hql(type = Logical.EQ)
	private String operatorType;

	/** 操作人员 */
	@Column(name = "oper_name")
	@Excel(name = "操作人员")
	@Hql(type = Logical.LIKE)
	private String operName;

	/** 部门名称 */
	@Column(name = "dept_name")
	@Excel(name = "部门名称")
	@Hql(type = Logical.LIKE)
	private String deptName;

	/** 请求url */
	@Column(name = "oper_url",length=1024)
	@Excel(name = "请求地址")
	@Hql(type = Logical.LIKE)
	private String operUrl;

	/** 操作地址 */
	@Column(name = "oper_ip")
	@Excel(name = "操作地址")
	@Hql(type = Logical.LIKE)
	private String operIp;

	/** 操作地点 */
	@Column(name = "oper_location")
	@Excel(name = "操作地点")
	@Hql(type = Logical.LIKE)
	private String operLocation;

	/** 请求参数 */
	@Column(name = "oper_param", length=4000)
	@Excel(name = "请求参数")
	@Hql(type = Logical.LIKE)
	private String operParam;

	/** 操作状态（0正常 1异常） */
	@Column(name = "status")
	@Dict(value = "sys_success_status")
	@Excel(name = "状态", readConverterExp = "0=成功,1=失败")
	@Hql(type = Logical.EQ)
	private String status;

	/** 错误消息 */
	@Column(name = "error_msg", length=4000)
	@Excel(name = "错误消息")
	@Hql(type = Logical.LIKE)
	private String errorMsg;

	/** 操作时间 */
	@Column(name = "create_time")
	@Excel(name = "操作时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

}
