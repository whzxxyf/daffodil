package com.daffodil.system.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.daffodil.core.annotation.Hql;
import com.daffodil.core.annotation.Hql.Logical;
import com.daffodil.core.entity.BaseEntity;
import com.daffodil.core.enums.OnlineStatus;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 当前在线会话
 * 
 * @author yweijian
 * @date 2019年8月16日
 * @version 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "sys_user_online")
public class SysUserOnline extends BaseEntity {
	private static final long serialVersionUID = -4030876295902768951L;

	/** 用户会话编号 */
	@Id
	@Column(name = "session_id")
	private String id;
	
	/** 用户编号 */
	@Column(name = "user_id")
	@Hql(type = Logical.EQ)
	private String userId;

	/** 登录名称 */
	@Column(name = "login_name")
	@Hql(type = Logical.LIKE)
	private String loginName;

	/** 登录IP地址 */
	@Column(name = "ipaddr")
	@Hql(type = Logical.LIKE)
	private String ipaddr;

	/** 登录地址 */
	@Column(name = "login_location")
	@Hql(type = Logical.LIKE)
	private String loginLocation;

	/** 浏览器类型 */
	@Column(name = "browser")
	@Hql(type = Logical.LIKE)
	private String browser;

	/** 操作系统 */
	@Column(name = "os")
	@Hql(type = Logical.LIKE)
	private String os;

	/** session创建时间 */
	@Column(name = "start_timestamp")
	private Date startTimestamp;

	/** session最后访问时间 */
	@Column(name = "last_access_time")
	private Date lastAccessTime;

	/** 超时时间，单位为分钟 */
	@Column(name = "expire_time")
	private Long expireTime;

	/** 在线状态 */
	@Column(name = "status")
	private OnlineStatus status;

}
