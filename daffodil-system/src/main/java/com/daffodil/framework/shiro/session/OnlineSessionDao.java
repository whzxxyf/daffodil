package com.daffodil.framework.shiro.session;

import java.io.Serializable;
import java.util.Date;

import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.eis.EnterpriseCacheSessionDAO;
import org.springframework.beans.factory.annotation.Autowired;

import com.daffodil.framework.shiro.service.SysShiroService;
import com.daffodil.util.StringUtils;


/**
 * 获取、删除、记录用户会话
 * @author yweijian
 * @date 2020年2月15日
 * @version 1.0
 */
public class OnlineSessionDao extends EnterpriseCacheSessionDAO{

	@Autowired
	private SysShiroService shiroService;
	
	private static final String ONLINE_SESSION_LAST_RECORD_TIMESTAMP = "ONLINE_SESSION_LAST_RECORD_TIMESTAMP";
	
	/**
	 * 获取会话
	 */
	@Override
    protected Session doReadSession(Serializable sessionId){
		OnlineSession onlineSession = shiroService.getOnlineSession(sessionId);
		return onlineSession;
    }

	/**
	 * 删除会话
	 */
	@Override
	protected void doDelete(Session session) {
		String sessionId = (String) session.getId();
		shiroService.deleteOnlineSession(sessionId);
	}
	
	/**
	 * 记录会话
	 */
	public void doRecord(OnlineSession onlineSession) {
		Date lastRecordTimestamp = (Date) onlineSession.getAttribute(ONLINE_SESSION_LAST_RECORD_TIMESTAMP );
		if (lastRecordTimestamp != null) {
			boolean needSync = true;
			long deltaTime = onlineSession.getLastAccessTime().getTime() - lastRecordTimestamp.getTime();
			//同步session到数据库的周期1分钟
			if (deltaTime < 1 * 60 * 1000) {
				// 时间差不足 无需同步
				needSync = false;
			}
			// session 数据变更了 同步
			if (StringUtils.isNotEmpty(onlineSession.getUserId()) && onlineSession.isAttributeChanged()) {
				needSync = true;
			}
			if (needSync == false) {
				return;
			}
		}
		// 更新上次同步数据库时间
		onlineSession.setAttribute(ONLINE_SESSION_LAST_RECORD_TIMESTAMP, onlineSession.getLastAccessTime());
		// 更新完后 重置标识
		if (onlineSession.isAttributeChanged()) {
			onlineSession.resetAttributeChanged();
		}
		shiroService.recordOnlineSession(onlineSession);
	}

}
