package com.daffodil;

import com.daffodil.jasypt.ui.JasyptUI;

/**
 * 配置文件加解密工具
 * @author yweijian
 * @date 2021年2月7日
 * @version 1.0
 * @description
 */
public class DaffodilJasyptApplication {
	
	public static void main(String[] args) {
		JasyptUI jasyptUI = new JasyptUI();
		jasyptUI.initJasypUI();
	}
}
