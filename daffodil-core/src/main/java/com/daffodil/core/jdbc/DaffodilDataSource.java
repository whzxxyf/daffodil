package com.daffodil.core.jdbc;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author yweijian
 * @date 2021年9月1日
 * @version 1.0
 * @description
 */
@Slf4j
public class DaffodilDataSource extends AbstractRoutingDataSource{

	@Override
	protected Object determineCurrentLookupKey() {
		String lookupKey = DaffodilDataSourceContextHolder.getDataSourceLookupKey();
		if(null != lookupKey) {
			log.info("当前使用数据源是：" + lookupKey);
		}
		return lookupKey;
	}

}
