package com.daffodil;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

import com.daffodil.core.jdbc.DaffodilDataSourceRegister;

/**
 * 多数据源demo
 * @author yweijian
 * @date 2021年8月31日
 * @version 1.0
 * @description
 */
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class})
@ComponentScan(basePackages = {"com.daffodil"})
//使用多数据源时必须将DaffodilDataSourceRegister引入注册
@Import(DaffodilDataSourceRegister.class)
public class DaffodilApplication extends SpringBootServletInitializer{

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(DaffodilApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(DaffodilApplication.class, args);
	}
	
}
